'use strict';

var redis = require("redis")
var Q = require("q")

module.exports = function (app) {
    var client;
    return {

        initialize: function (cb) {
            client = redis.createClient();
            client.on("ready", function () {
                app.internalStore = {}
                cb();
            })

            app.once('lower', function () {
                client.end();
            })
        },
        defaults: {
            __configKey__: {
                name: "interalStore"
            }
        },

        get: function (key) {
            var defer = Q.defer();
            client.hgetall(key, function (err, obj) {
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(obj);
            });

            return defer.promise;

        },
        set: function (key, value) {
            if (!value) {
                client.del(key);
                return
            }
            client.hmset(key, value);
        }

    };
};
