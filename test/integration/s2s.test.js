'use strict';

var expect = require('chai').expect;
var lifecycle = require('../helper/lifecycle.helper');

describe('Integration :: ', function() {

  var server1, server2;

  before(function (done) {
    lifecycle.setup2Servers(function(err, result) {
      if (err) {
        return done(err);
      }
      server1 = result.server1;
      server2 = result.server2;
      done();
    })
  });

  after(function (done) {
    lifecycle.teardown(server1, function () {
      lifecycle.teardown(server2, done);
    })
  });

  it('two servers should be created', function () {
    expect(server1).to.be.ok;
    expect(server2).to.be.ok;
  });

  it('both servers should have redis', function () {
    expect(server1.hooks.redis).to.be.ok;
    expect(server1.hooks.redis.get).to.be.a('function');
    expect(server1.hooks.redis.get).to.be.a('function');
    expect(server2.hooks.redis).to.be.ok;
    expect(server2.hooks.redis.get).to.be.a('function');
    expect(server2.hooks.redis.set).to.be.a('function');
  });

  it('should set in one a get in another', function (done) {
    server1.hooks.redis.set('test1', { hello : "world "});
    // 2 seconds should be enought to receive event
    server2.hooks.redis.get("test1").then(function(result){
      expect(result).to.eql({ hello : "world "})
      done();
    })
  });

  it('should set in one a get in but reverse', function (done) {
      server2.hooks.redis.set('test2', { hello : "world "});
      server1.hooks.redis.get("test2").then(function(result){
        expect(result).to.eql({ hello : "world "})
        done();
      })
  });

  it('should set and get same server', function (done) {
    server1.hooks.redis.set('test3', { hello : "world "});
    // 2 seconds should be enought to receive event
    server1.hooks.redis.get("test3").then(function(result){
      expect(result).to.eql({ hello : "world "})
      done();
    })
  });

  it('should merge the dict', function (done) {
    // 2 seconds should be enought to receive event
    server1.hooks.redis.set('testUpdate',{a:'1'});
    server1.hooks.redis.set('testUpdate',{b:'2'});
    server1.hooks.redis.get("testUpdate").then(function(result){
      expect(result).to.eql({a:'1',b:'2'})
      done();
    }).catch(function(err){
      done(err);
    })
  });

  it('should pass back null on empty get', function (done) {
    // 2 seconds should be enought to receive event
    server1.hooks.redis.set('testdel',{a:1});
    server1.hooks.redis.set('testdel');
    server1.hooks.redis.get("testdel").then(function(result){
      expect(result).to.not.be.ok
      done();
    })
  });


});
