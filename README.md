# Sails.js Redis Store hook

**This hook is in development and has very limited functionnality please feel free to create issues and new features requests**


## Send events

### Set and get objects

To set

If nothing is passed in the key is unset

```javascript
sails.internalStore.set("string",[{}])
```

To get

return promise

If nothing is present for key then null is returned

```javascript
sails.internalStore.get('string').then(function(result){})
```

# License

MIT
